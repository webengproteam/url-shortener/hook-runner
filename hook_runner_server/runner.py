import datetime
import requests
import json
from smtplib import SMTP
from email.message import EmailMessage
from email.headerregistry import Address

from hook_runner_server.config import BaseConfig

from hook_runner_server.db.config import db

from hook_runner_server.db.models import HookTrigger
from hook_runner_server.db.models import UriAccess
from hook_runner_server.db.models import HookActivation

REQUEST_TIMEOUT = 3


def check_all():
    try:
        print('Checking')
        for trigger in HookTrigger.query.filter_by(active=True).all():
            if check_trigger(trigger):
                print(f'TRIGGER (id: {trigger.id}, uri: {trigger.uri})')
                for action in trigger.actions_http:
                    exec_action_http(action)
                for action in trigger.actions_email:
                    exec_action_mail(action)
    except Exception as e:
        print(f'Exception happened: {e}')


def check_trigger(trigger):
    # Time from which clicks affect this trigger
    earliest_click_time = datetime.datetime.now() - datetime.timedelta(minutes=trigger.minutes)
    # Time from which clicks have not already triggered a hook
    last_activation = HookActivation.query.\
        filter_by(hook=trigger).\
        order_by(HookActivation.datetime.desc()).\
        first()

    if last_activation:
        last_activation_time = last_activation.datetime
    else:
        last_activation_time = datetime.datetime(year=1900, month=1, day=1, hour=1)

    accesses = UriAccess.query.\
        filter(UriAccess.datetime > last_activation_time).\
        filter(UriAccess.datetime >= earliest_click_time).\
        filter_by(uri=trigger.uri).\
        order_by(UriAccess.datetime.asc()).\
        all()

    clicks = accesses.__len__()

    if clicks >= trigger.clicks:
        time_delta = accesses[-1].datetime - accesses[0].datetime
        minutes = time_delta.seconds // 60
        minutes = minutes if minutes > 0 else 1

        activation = HookActivation(
            datetime=datetime.datetime.now(),
            clicks=clicks,
            minutes=minutes,
            hook=trigger
        )
        db.session.add(activation)
        db.session.commit()

        return True
    else:
        return False


def exec_action_http(action):
    params = f'?{action.http_params}' if action.http_params else ''
    uri = f'{action.http_endp}{params}'
    verb = action.http_verb
    data = json.loads(action.http_body) if action.http_body else None

    try:
        if verb == 'GET':
            requests.get(uri, timeout=REQUEST_TIMEOUT)
        elif verb == 'PUT':
            requests.put(uri, data=data, timeout=REQUEST_TIMEOUT)
        elif verb == 'POST':
            requests.post(uri, data=data, timeout=REQUEST_TIMEOUT)
        elif verb == 'DELETE':
            requests.delete(uri, timeout=REQUEST_TIMEOUT)
        else:
            pass
        
        print(f'EXEC HTTP (uri: {uri}, verb: {verb}, data: {data}')
    except Exception as e:
        print(f'Exception on HTTP request: {e}')


def exec_action_mail(action):
    try:
        with SMTP(host=BaseConfig.MAIL_SERVER, port=BaseConfig.MAIL_PORT) as smtp:
            if BaseConfig.MAIL_TLS:
                smtp.starttls()

            if BaseConfig.MAIL_AUTH:
                smtp.login(user=BaseConfig.MAIL_USER, password=BaseConfig.MAIL_PASS)

            msg = EmailMessage()
            msg['Subject'] = action.subject
            msg['From'] = Address(addr_spec='url-shortener@mail.com')
            msg['To'] = tuple([Address(addr_spec=address) for address in action.address.split(',')])
            msg.set_content(action.body)

            smtp.send_message(msg=msg)
        
        print(f'EXEC MAIL (To: {action.address}, Subject: {action.subject}, Body: {action.body})')
    except Exception as e:
        print(f'Exception on SMTP send message: {e}')

