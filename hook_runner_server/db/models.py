import json


from hook_runner_server.db.config import db


class Users(db.Model):
    __table_name__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.Text, unique=True, nullable=False)
    passwd_hash = db.Column(db.Text, nullable=False)

    def __repr__(self):
        return f'<User {self.email}>'


class Uri(db.Model):
    __table_name__ = 'uri'

    short_uri = db.Column(db.String(20), primary_key=True)
    long_uri = db.Column(db.String(100), nullable=False)
    accessible = db.Column(db.Boolean)
    active = db.Column(db.Boolean, nullable=False)
    secure = db.Column(db.Boolean)
    admin = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    user = db.relationship('Users', backref=db.backref('uris', lazy=True))

    short_uri_length = 8

    def __repr__(self):
        return f'<URI {self.short_uri}>'


class UriAccess(db.Model):
    __table_name__ = 'uri_access'

    id = db.Column(db.Integer, primary_key=True)
    datetime = db.Column(db.DateTime, nullable=False)
    browser = db.Column(db.Text)
    os = db.Column(db.Text)
    ip = db.Column(db.Text)
    uri = db.Column(db.String(20), db.ForeignKey('uri.short_uri'), nullable=False)

    def __repr__(self):
        return f'<UriAccess {self.id}>'


class HookTrigger(db.Model):
    __table_name__ = 'hook_trigger'

    id = db.Column(db.Integer, primary_key=True)
    clicks = db.Column(db.Integer)
    minutes = db.Column(db.Integer)
    admin = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    user = db.relationship('Users', backref=db.backref('hooks', lazy=True))
    uri = db.Column(db.String(20), db.ForeignKey('uri.short_uri'), nullable=False)
    uri_ref = db.relationship('Uri', backref=db.backref('hooks', lazy=True))
    active = db.Column(db.Boolean, nullable=False)

    def __repr__(self):
        return f'<HookTrigger {self.id}>'


class HookActionHttp(db.Model):
    __table_name__ = 'hook_action_http'

    id = db.Column(db.Integer, primary_key=True)
    http_endp = db.Column(db.Text)
    http_verb = db.Column(db.Text)
    http_params = db.Column(db.Text)
    http_body = db.Column(db.Text)
    hook_id = db.Column(db.Integer, db.ForeignKey('hook_trigger.id'), nullable=False)
    hook = db.relationship('HookTrigger', backref=db.backref('actions_http', lazy=True))

    def __repr__(self):
        return f'<HookActionHTTP {self.id}>'


class HookActionEmail(db.Model):
    __table_name__ = 'hook_action_email'

    id = db.Column(db.Integer, primary_key=True)
    address = db.Column(db.Text)
    subject = db.Column(db.Text)
    body = db.Column(db.Text)
    hook_id = db.Column(db.Integer, db.ForeignKey('hook_trigger.id'), nullable=False)
    hook = db.relationship('HookTrigger', backref=db.backref('actions_email', lazy=True))

    def __repr__(self):
        return f'<HookActionEmail {self.id}>'


class HookActivation(db.Model):
    __table_name__ = 'hook_activation'

    id = db.Column(db.Integer, primary_key=True)
    datetime = db.Column(db.DateTime, nullable=False)
    clicks = db.Column(db.Integer)
    minutes = db.Column(db.Integer)
    hook_id = db.Column(db.Integer, db.ForeignKey('hook_trigger.id'), nullable=False)
    hook = db.relationship('HookTrigger', backref=db.backref('activations', lazy=True))

    def __repr__(self):
        return f'<HookActionEmail {self.id}>'
