from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def config_db(app):
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    db.init_app(app)
