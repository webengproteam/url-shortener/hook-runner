import datetime
import requests
import json
import threading

from flask import request

from hook_runner_server.config import BaseConfig

from hook_runner_server.db.config import db

from hook_runner_server.db.models import HookTrigger
from hook_runner_server.db.models import HookActionHttp
from hook_runner_server.db.models import HookActionEmail
from hook_runner_server.db.models import UriAccess
from hook_runner_server.db.models import HookActivation

MAX_TIMEOUT = 1
PORT = 8000
HOST = f'http://localhost:{PORT}'
TEST_URI = 'abc123'

# Conditions
cnd_1 = threading.Condition()
cnd_2 = threading.Condition()
cnd_3 = threading.Condition()
cnd_4 = threading.Condition()
cnd_5 = threading.Condition()
cnd_6 = threading.Condition()

cnd_1_mail = threading.Condition()

# Variables
test_1_param_key = 'hey'
test_1_param_value = 'there'
test_1_params = f'{test_1_param_key}={test_1_param_value}'
test_2_body = {"hey": "ho", "lets": 0}


def run_tests(app):
    print('Preparing test server')

    # Endpoints
    @app.route(endp_name(1), methods=['GET'])
    def endp_1():
        if request.method == 'GET' and request.args[test_1_param_key] == test_1_param_value:
            with cnd_1:
                cnd_1.notify()
            return 'Ok'
        else:
            return 'Bad'

    @app.route(endp_name(2), methods=['POST'])
    def endp_2():
        if request.method == 'POST' and request.form:
            with cnd_2:
                cnd_2.notify()
            return 'Ok'
        else:
            return 'Bad'

    @app.route(endp_name(3), methods=['PUT'])
    def endp_3():
        if request.method == 'PUT' and request.form:
            with cnd_3:
                cnd_3.notify()
            return 'Ok'
        else:
            return 'Bad'

    @app.route(endp_name(4), methods=['GET'])
    def endp_4():
        if request.method == 'GET':
            with cnd_4:
                cnd_4.notify()
            return 'Ok'
        else:
            return 'Bad'

    @app.route(endp_name(5), methods=['GET'])
    def endp_5():
        if request.method == 'GET':
            with cnd_5:
                cnd_5.notify()
            return 'Ok'
        else:
            return 'Bad'

    @app.route(endp_name(6), methods=['GET'])
    def endp_6():
        if request.method == 'GET':
            with cnd_6:
                cnd_6.notify()
            return 'Ok'
        else:
            return 'Bad'

    print('Creating test endpoints thread')
    http_endpoints = threading.Thread(target=start_http_server, args=(app,), daemon=True)
    http_endpoints.start()
    mail_endpoints = threading.Thread(target=start_mail_server, daemon=True)
    mail_endpoints.start()

    print('Adding click logs to database')
    create_clicks()

    print('\n-- Running tests --')
    errors = 0

    errors += test_1()
    errors += test_2()
    errors += test_3()
    errors += test_4()
    errors += test_5()
    errors += test_6()
    errors += test_1_mail()

    print('\n-- Finished tests --')
    return errors


class SMTPTestServer:
    async def handle_DATA(self, server, session, envelope):
        with cnd_1_mail:
            cnd_1_mail.notify()
        return '250 OK'


def start_http_server(app):
    # This is needed because the simple devel server of Flask can't be ran in a thread
    from tornado.wsgi import WSGIContainer
    from tornado.web import FallbackHandler, Application
    from tornado.ioloop import IOLoop
    from aiosmtpd.controller import Controller
    import asyncio

    # Initialize asyncio loop
    asyncio.set_event_loop(asyncio.new_event_loop())

    # Create HTTP server
    cont = WSGIContainer(app)
    application = Application([
        (r"/(.*)", FallbackHandler, dict(fallback=cont)),
    ])
    application.listen(PORT)
    IOLoop.instance().start()


def start_mail_server():
    from aiosmtpd.controller import Controller
    import asyncio

    # Initialize asyncio loop
    asyncio.set_event_loop(asyncio.new_event_loop())

    # Create mail server
    controller = Controller(
        SMTPTestServer(),
        hostname=BaseConfig.MAIL_SERVER,
        port=BaseConfig.MAIL_PORT
    )

    controller.start()


def endp_name(i):
    return f'/endp_{i}'


def wait_condition(condition):
    with condition:
        if condition.wait(timeout=MAX_TIMEOUT):
            print('OK')
            return 0
        else:
            print('FAIL')
            return 1


def wait_no_condition(condition):
    with condition:
        if not condition.wait(timeout=MAX_TIMEOUT):
            print('OK')
            return 0
        else:
            print('FAIL')
            return 1


def wait_multi_condition(condition, times):
    for i in range(times):
        with condition:
            if not condition.wait(timeout=MAX_TIMEOUT):
                print('FAIL')
                return 1
    print('OK')
    return 0


def clean_db():
    # Clean all the hooks and clicks in the database
    clicks = UriAccess.query.all()
    hooks = HookTrigger.query.all()
    activations = HookActivation.query.all()

    for click in clicks:
        db.session.delete(click)
    for activation in activations:
        db.session.delete(activation)
    for hook in hooks:
        for action in hook.actions_http:
            db.session.delete(action)
        for action in hook.actions_email:
            db.session.delete(action)
        db.session.delete(hook)

    db.session.commit()


def create_clicks():
    # There are 30 clicks for the default URI, one per minute, from now till now-29
    for minute in range(30):
        db.session.add(
            UriAccess(
                datetime=datetime.datetime.now() - datetime.timedelta(minutes=minute),
                uri=TEST_URI
            )
        )
    db.session.commit()


def test_1():
    print('\nTEST 1')
    print('======')
    print('Active hook. Valid triggering conditions. Exec single HTTP GET action.')

    ENDPOINT = endp_name(1)

    hook = HookTrigger(
        clicks=5,
        minutes=10,
        admin=1,
        uri=TEST_URI,
        active=True
    )

    action_http = HookActionHttp(
        http_endp=f'{HOST}{ENDPOINT}',
        http_verb='GET',
        http_params=test_1_params,
        hook=hook
    )

    db.session.add(hook)
    db.session.commit()

    return wait_condition(cnd_1)


def test_2():
    print('\nTEST 2')
    print('======')
    print('Active hook. Valid triggering conditions. Exec single HTTP POST action.')

    ENDPOINT = endp_name(2)

    hook = HookTrigger(
        clicks=5,
        minutes=10,
        admin=1,
        uri=TEST_URI,
        active=True
    )

    action_http = HookActionHttp(
        http_endp=f'{HOST}{ENDPOINT}',
        http_verb='POST',
        http_params=None,
        http_body=json.dumps(test_2_body),
    )

    hook.actions_http.append(action_http)

    db.session.add(hook)
    db.session.commit()

    return wait_condition(cnd_2)


def test_3():
    print('\nTEST 3')
    print('======')
    print('Active hook. Valid triggering conditions. Exec single HTTP PUT action.')

    ENDPOINT = endp_name(3)

    hook = HookTrigger(
        clicks=5,
        minutes=10,
        admin=1,
        uri=TEST_URI,
        active=True
    )

    action_http = HookActionHttp(
        http_endp=f'{HOST}{ENDPOINT}',
        http_verb='PUT',
        http_params=None,
        http_body=json.dumps(test_2_body),
    )

    hook.actions_http.append(action_http)

    db.session.add(hook)
    db.session.commit()

    return wait_condition(cnd_3)


def test_4():
    print('\nTEST 4')
    print('======')
    print('Inactive hook. Valid triggering conditions. No exec HTTP GET action.')

    ENDPOINT = endp_name(4)

    hook = HookTrigger(
        clicks=5,
        minutes=10,
        admin=1,
        uri=TEST_URI,
        active=False
    )

    action_http = HookActionHttp(
        http_endp=f'{HOST}{ENDPOINT}',
        http_verb='GET',
        http_params=test_1_params,
        hook=hook
    )

    db.session.add(hook)
    db.session.commit()

    return wait_no_condition(cnd_4)


def test_5():
    print('\nTEST 5')
    print('======')
    print('Active hook. Valid triggering conditions. Exec multiple HTTP GET action.')

    ENDPOINT = endp_name(5)

    hook = HookTrigger(
        clicks=5,
        minutes=10,
        admin=1,
        uri=TEST_URI,
        active=True
    )

    action_http = HookActionHttp(
        http_endp=f'{HOST}{ENDPOINT}',
        http_verb='GET',
        http_params=test_1_params,
        hook=hook
    )

    action_http2 = HookActionHttp(
        http_endp=f'{HOST}{ENDPOINT}',
        http_verb='GET',
        http_params=test_1_params,
        hook=hook
    )

    db.session.add(hook)
    db.session.commit()

    return wait_multi_condition(cnd_5, 2)


def test_6():
    print('\nTEST 6')
    print('======')
    print('Active hook. Invalid triggering conditions. No exec HTTP GET action.')

    ENDPOINT = endp_name(6)

    hook = HookTrigger(
        clicks=500,
        minutes=10,
        admin=1,
        uri=TEST_URI,
        active=True
    )

    action_http = HookActionHttp(
        http_endp=f'{HOST}{ENDPOINT}',
        http_verb='GET',
        http_params=test_1_params,
        hook=hook
    )

    db.session.add(hook)
    db.session.commit()

    return wait_no_condition(cnd_6)


def test_1_mail():
    print('\nTEST 1 MAIL')
    print('======')
    print('Active hook. Valid triggering conditions. Sends email.')

    ENDPOINT = endp_name(1)

    hook = HookTrigger(
        clicks=5,
        minutes=10,
        admin=1,
        uri=TEST_URI,
        active=True
    )

    action_email = HookActionEmail(
        address='test@mail.com',
        subject='Subject',
        body='Body',
        hook=hook
    )

    db.session.add(hook)
    db.session.commit()

    return wait_condition(cnd_1_mail)
