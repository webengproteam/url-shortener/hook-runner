#!/usr/bin/env python3

from flask import Flask
import time
import sys

from hook_runner_server.db.config import config_db
from hook_runner_server.config import BaseConfig
from hook_runner_server.runner import check_all




def initialize_app():
    app = Flask(__name__)
    app.config.from_object(BaseConfig)
    config_db(app)
    return app


def main(verbose = True):
    if not verbose:
        class NullDevice():
            def write(self, s):
                pass
        sys.stdout = NullDevice()
    
    app = initialize_app()
    check_loop(app)


def check_loop(app):
    app.app_context().push()
    while True:
        time.sleep(0.5)
        check_all()


def test():
    from multiprocessing import Process
    from hook_runner_server.test import run_tests
    from hook_runner_server.test import clean_db

    print('--------------------------------')
    print('--HOOK RUNNER TEST ENVIRONMENT--')
    print('--------------------------------')

    print('Starting main server process')
    server = Process(target=main, args=(False,))
    server.start()

    print('Initializing test app')
    app = initialize_app()
    app.app_context().push()

    print('Cleaning old data from database')
    clean_db()

    errors = run_tests(app)

    print('Terminating main server process')
    server.terminate()
    server.join()

    if errors == 0:
        print('\n\nTests completed successfully')
        exit(0)
    else:
        print(f'\n\nTests completed with {errors} errors')
        exit(1)


if __name__ == '__main__':
    import sys

    if sys.argv.__len__() > 1 and sys.argv[1] == 'test':
        test()
    else:
        main()
