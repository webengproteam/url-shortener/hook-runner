# Hook runner

**Master:** [![pipeline status](https://gitlab.com/webengproteam/url-shortener/hook-runner/badges/master/pipeline.svg)](https://gitlab.com/webengproteam/url-shortener/hook-runner/commits/master)
**Devel:** [![pipeline status](https://gitlab.com/webengproteam/url-shortener/hook-runner/badges/devel/pipeline.svg)](https://gitlab.com/webengproteam/url-shortener/hook-runner/commits/devel)

Server in charge of checking trigger conditions of hooks and launching them.


## Local development

To set up the database run:

```bash
source set_db.sh
```

Then set the proper environmental variables with:

```bash
source set_env.sh local
```

At this point you can run the tests with:

```bash
python3 -m hook_runner_server test
```

Or run the normal app with:

```bash
python3 -m hook_runner_server
```

Later you can clean the database with:

```bash
source unset_db.sh
```
