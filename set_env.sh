#!/usr/bin/env sh

if [ ! -z $1 ]
then
  PARAM=$1
else
  PARAM='none'
fi

source $PWD/.env
export $(cat .env | grep -v ^# |  cut -d= -f1)

# When executable param is 'local' the db host is set to localhost
if [ $PARAM = 'local' ]
then
  export DB_SERVICE=localhost
  export MAIL_SERVER=localhost
  export MAIL_PORT=5870
  export MAIL_TLS=False
  export MAIL_AUTH=False
fi